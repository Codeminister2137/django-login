# Django Login

## Table of contents

* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Roadmap](#roadmap)
* [Project status](#project-status)

## General info
<details>
<summary>Click here to see general information about <b>Task Scheduler</b>!</summary>
<b>This is a portfolio project focused on authorization in Django</b>.
User can create an account or log in with existing one. There is a possibility of
changing all user's information when desired, including password. Front-end of the application is not the focus here so it is very basic.

</details>

## Technologies
<details>
<summary>Click here to see technologies used in the project</summary>
<ul>
<li>Django</li>
<li>Sqlite</li>
</ul>

</details>

## Setup
<details>
<summary>Here will be a guide for setting up the app yourself</summary>
Placeholder

</details>

## Roadmap
<details>
<summary>Click here to see project roadmap</summary>
<ul>
<li> [x] Create basic structure</li>
<li> [x] Add login and registration</li>
<li> [x] Allow users to reset their password and edit their profile</li>
</ul>
</details>



## Project status
Finished
