from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.views import PasswordResetView
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views import View

from .forms import (
    CustomUserChangeForm,
    CustomUserCreationForm,
    LoginForm,
    ProfileChangeForm,
)

# Create your views here.


def home(request):
    return render(request, "user/index.html")


class Signup(View):
    form = CustomUserCreationForm
    template = "user/signup.html"

    def get(self, request):
        return render(request, self.template, {"form": self.form})

    def post(self, request):
        form = self.form(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Your account has been successfully created")
            return redirect("home")
        messages.error(request, "Your account has not been created")
        return render(request, self.template, {"form": self.form})


class Login(View):
    form = LoginForm
    template = "user/login.html"

    def get(self, request):
        return render(request, self.template, {"form": self.form})

    def post(self, request):
        form = self.form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
                messages.success(
                    request,
                    f"Hello {username.title()}, you have been successfully logged in",
                )
                return redirect("home")

        messages.error(request, "Check your username and password again")
        return render(request, self.template, {"form": self.form})


class Logout(View):
    def get(self, request):
        logout(request)
        messages.success(request, "You have been successfully logged out")
        return redirect("login")


class Profile(View):
    template = "user/profile.html"
    user_form = CustomUserChangeForm
    profile_form = ProfileChangeForm

    def get(self, request):
        return render(
            request,
            self.template,
            {
                "user_form": self.user_form(instance=request.user),
                "profile_form": self.profile_form(instance=request.user.profile),
            },
        )

    def post(self, request):
        user_form = self.user_form(request.POST, instance=request.user)
        profile_form = self.profile_form(
            request.POST, request.FILES, instance=request.user.profile
        )
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, "Profile has been successfully updated")
            return redirect("profile")
        messages.error(request, "Something went wrong, check your input data again")
        return render(
            request,
            self.template,
            {"user_form": self.user_form, "profile_form": self.profile_form},
        )


class ResetPassword(SuccessMessageMixin, PasswordResetView):
    template_name = "user/password_reset.html"
    email_template_name = "user/password_reset_email.html"
    subject_template_name = "user/password_reset_subject.txt"
    success_message = (
        "We've emailed you instructions for setting your password, "
        "if an account exists with the email you entered. You should receive them shortly."
        " If you don't receive an email, "
        "please make sure you've entered the address you registered with, and check your spam folder."
    )
    success_url = reverse_lazy("home")
