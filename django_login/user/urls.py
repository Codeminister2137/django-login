from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.urls import path

from . import views

urlpatterns = [
    path("", login_required(views.home), name="home"),
    path("signup", views.Signup.as_view(), name="signup"),
    path("login", views.Login.as_view(), name="login"),
    path("logout", login_required(views.Logout.as_view()), name="logout"),
    path("profile", login_required(views.Profile.as_view()), name="profile"),
    path("password_reset", views.ResetPassword.as_view(), name="password_reset"),
    path(
        "password_reset_confirm/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="user/password_reset_confirm.html"
        ),
        name="password_reset_confirm",
    ),
    path(
        "password-reset-complete/",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="user/password_reset_complete.html"
        ),
        name="password_reset_complete",
    ),
]
