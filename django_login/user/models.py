from django.contrib.auth.models import AbstractUser
from django.db import models
from PIL import Image

# Create your models here.


class CustomUser(AbstractUser):
    pass

    def __str__(self):
        return self.username


class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    image = models.ImageField(default="default.jpeg", upload_to="profile_pics")

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        new_image = Image.open(self.image.path)
        output_size = (300, 300)
        new_image.thumbnail(output_size)
        new_image.save(self.image.path)

    def __str__(self):
        return f"Profile of {self.user.username}"
